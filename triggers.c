/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-06-01T23:05:01+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <string.h>
#include <stdint.h>

#include "triggers.h"
#include "driver.h"

// #define DEBUG

static uint8_t s_pin_index;
static enumerate_pins_ptr on_enumerate_pins;
static io_port_t on_port;

static volatile uint_fast16_t s_trigger_port;

static const periph_pin_t aux_out[] = {
  { .function = Output_Aux0, .pin = HW_TRIGGER_PIN, .group = PinGroup_AuxOutput, .description = "Hardware Trigger" }, // P0
  { .function = Output_Aux1, .pin = PAUSE_RESUME_PIN, .group = PinGroup_AuxOutput, .description = "Pause/Resume" } // P1
};
#define AUX_OUT_SIZE (sizeof (aux_out) / sizeof (aux_out[0]))

void trigger_arm(uint_fast16_t mask)
{
  bool arm = s_trigger_port == 0;
  hal.set_bits_atomic(&s_trigger_port, mask);
  if (arm)
  {
    timer[STEPPER_TIMER].irq_enable = 0;
  }
}

void trigger_disarm(uint_fast16_t mask)
{
  if (hal.clear_bits_atomic(&s_trigger_port, mask) == mask)
  {
    timer[STEPPER_TIMER].irq_enable = 1;
  }
}

void trigger_trig(uint_fast16_t mask, bool hardware)
{
  if (hal.clear_bits_atomic(&s_trigger_port, mask) == mask)
  {
    timer[STEPPER_TIMER].value = 0;
    timer[STEPPER_TIMER].irq_enable = 1;
    if (hardware)
      hal.stepper.interrupt_callback();
  }
}

static void digital_out(uint8_t port, bool on)
{
  port -= s_pin_index;
  if (port >= AUX_OUT_SIZE)
  {
    if (on_port.digital_out)
      on_port.digital_out(port, on);
    return;
  }

#ifdef DEBUG
  printf("port(%s) = %s\n", aux_out[port].description, on ? "on" : "off");
#endif

  if (on)
    trigger_arm(TRIGGER_MASK(aux_out[port].pin));
  else
    trigger_disarm(TRIGGER_MASK(aux_out[port].pin));
}

static xbar_t *get_pin_info(io_port_type_t type, io_port_direction_t dir, uint8_t port)
{
  static xbar_t pin;
  xbar_t *info = NULL;


  if (type == Port_Digital && dir == Port_Output && port < AUX_OUT_SIZE)
  {
    memset(&pin, 0, sizeof(xbar_t));
    pin.mode.output = On;
    pin.function = aux_out[port].function;
    pin.group = aux_out[port].group;
    pin.pin = aux_out[port].pin;
    pin.bit = 1 << aux_out[port].pin;
    pin.port = (void *) aux_out[port].port;
    pin.description = aux_out[port].description;
    info = &pin;
  }
  else if (on_port.get_pin_info)
    return on_port.get_pin_info(type, dir, port);
  
  return info;
}

static void enumerate_pins (bool low_level, pin_info_ptr pin_info)
{
  size_t i;

  if (on_enumerate_pins)
    on_enumerate_pins(low_level, pin_info);

  for (i = 0; i < AUX_OUT_SIZE; ++i)
    pin_info(get_pin_info(Port_Digital, Port_Output, i));
}

void triggers_init()
{
  s_trigger_port = 0;
  s_pin_index = hal.port.num_digital_out;
  on_port = hal.port;

  hal.port.num_digital_out += AUX_OUT_SIZE;
  hal.port.digital_out = digital_out;
  hal.port.get_pin_info = get_pin_info;

  on_enumerate_pins = hal.enumerate_pins;
  hal.enumerate_pins = enumerate_pins;
}
